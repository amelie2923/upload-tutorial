<?php

namespace App\Model\Table;
use Cake\ORM\Table;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Imagick\Imagine;

class UsersTable extends Table{
    public function initialize(array $config)
    {
        $this->setTable('users');
        $this->setDisplayField('username');
        $this->setPrimaryKey('id');

        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'photo' => [
                'fields' => [
                    'dir' => 'photo_dir',
                    'size' => 'photo_size',
                    'type' => 'photo_type'
                ],
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    //convertit tous les caractères en minuscule
                    return strtolower($data['name']);
                },
                // 'transformer' =>  function ($table, $entity, $data, $field, $settings) {
                    'transformer' => function (\Cake\Datasource\RepositoryInterface $table, \Cake\Datasource\EntityInterface $entity, $data, $field, $settings) {
                    $extension = pathinfo($data['name'], PATHINFO_EXTENSION);

                    //stocke le thumbnail dans un fichier temporaire
                    $tmp = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;

                    //utilise la librairie Imagine pour créer le thumbnail
                    $size = new \Imagine\Image\Box(60, 60);
                    $mode = \Imagine\Image\ImageInterface::THUMBNAIL_INSET;
                    $imagine = new \Imagine\Gd\Imagine();

                    //sauvegarde le fichier modifié dans le fichier temp
                    $imagine->open($data['tmp_name'])
                        ->thumbnail($size, $mode)
                        ->save($tmp);

                    //retourne l'original et le thumbnail
                    return [
                        $data['tmp_name'] => $data['name'],
                        $tmp => 'thumbnail-' . $data['name'],
                    ];
                },

                'deleteCallback' => function ($path, $entity, $field, $settings) {
                    //quand on supprime une entité, l'original et le thumbnail seront supprimés
                    return [
                        $path . $entity->{$field},
                        $path . 'thumbnail-' . $entity->{$field}
                    ];
                },
                'keepFilesOnDelete' => false
            ]
        ]);
    }
}